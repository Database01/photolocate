<?php namespace model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Admin extends Model{

    public $table = 'Admin';
    public $idTable = 'id';
    public $timestamps = false;

}