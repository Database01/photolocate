<?php

namespace controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use model\User;
use \exception\AuthException;

class Authentication {

    public static function authenticate($mail, $password) {
        try {
            $user = User::where("mail", "=", $mail)->firstOrFail();
            if (!password_verify($password, $user->password)) {
                throw new AuthException();
            }

        } catch(ModelNotFoundException $e) {
            throw new AuthException();
        }
    }

    public static function loadProfile($mail) {
        try {
            $membre = User::where("mail", "=", $mail)->firstOrFail();
            $_SESSION['connecte'] = true;
            $_SESSION['id'] = $membre->id;
            $_SESSION['username'] = $membre->username;
            $_SESSION['mail'] = $membre->mail;
            if($membre->admin == "1") {
                $_SESSION['admin'] = true;
            }
        } catch(ModelNotFoundException $e) {
            throw new AuthException();
        }
    }

    public static function checkAccessRights($required) {

    }

}