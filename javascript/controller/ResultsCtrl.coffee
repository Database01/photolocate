app = angular.module 'app'

#Affichage de la map et logique métier
app.controller 'ResultsCtrl', ['$scope', '$http', '$q', 'checkToken', '$timeout', '$rootScope', '$routeParams', '$location', ($scope, $http, $q, checkToken, $timeout, $rootScope, $routeParams, $location) ->


    # Vérifier le token
    checkToken('/choose-city')

    # Affichage du nom du joueur
    if localStorage.username != undefined
        $scope.user = {}
        $scope.user.username = localStorage.username


    url = rootpath+'/play/games/'+localStorage.id_game+'/leaderboard'
    $http.get(url)
        .success (data) ->
            $timeout ->
                $scope.leaderboard = data.results.leaderboard
                $scope.playerboard = data.results.playerboard
                $scope.totalPlayed = data.results.game.totalPlays
                $scope.user.score = data.results.player.score
                $scope.user.rank = data.results.player.rank
                $scope.user.id = data.results.player.id
                $scope.cityName = data.results.game.city
                $scope.difficultyName = data.results.game.difficulty
        .error ->
            modal = {scope:$scope, status:true,title:"Erreur", message: "Leaderboard unreachable", button1:"Redirection",pressed1:'/choose-city'}
            $rootScope.$broadcast('modal', modal)


    # Garder le score dans le scope
#    $scope.score="23423"

    # Récupérer le classement grâce à l'ID du jeu, le placer dans le scope
#    $scope.rank="44"

    # Récupérer le nombre total de parties jouées et terminées sur cette difficulté
#    $scope.totalPlayed="233"

    # Récupérer un tableau de chaque joueur avec son score, par ordre décroissant


    $scope.replay = ->
        $location.path '/choose-city'
]