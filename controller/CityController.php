<?php

namespace controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use model\City;
use model\User;
use \exception\AuthException;
use view\CityEditView;
use view\CityView;

class CityController extends BaseController {

    public function index() {
        checkAdmin();
        $cities = City::all();
        foreach ($cities as $city) {
            $city['url'] = $this->app->urlFor('adminCityEdit', array('id' => $city->id));
        }

        $view = new CityView();
        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $view->addVar('cities', $cities);
        $view->addVar('title', 'PhotoLocate - Administration');
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

    public function edit($id) {
        checkAdmin();
        $city = City::find($id);

        $view = new CityEditView();
        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $view->addVar('city', $city);
        $view->addVar('formPost', $this->app->urlFor('adminCityEditPost', array('id' => $city->id)));
        $token = generateToken();
        $view->addVar('token', $token);
        $_SESSION['token'] = $token;
        $view->addVar('title', 'PhotoLocate - Administration');
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

    public function editPost($id) {
        checkAdmin();
        $post = $this->app->request->post();

        if($_SESSION['token'] != $post['token']) {
            $this->app->flash('error', 'Bad token');
            $this->app->redirect($this->app->urlFor('adminCityEdit', array('id' => $id)));
        }


        $city = City::find($id);
        if($city == null) {
            $this->app->flash('error', 'Unknow city');
            $this->app->redirect($this->app->urlFor('adminCity'));
        }

        if(array_key_exists("delete", $post)) {
            $city->delete();
            $this->app->flash('success', 'Lieu supprimé avec succès');
            $this->app->redirect($this->app->urlFor('adminCity'));
        } else {
            $city->name = $post['name'];
            $city->longitude = $post['longitude'];
            $city->latitude = $post['latitude'];
            $city->zoom = $post['zoom'];
            $city->image = $post['image'];
            $city->save();
            $this->app->flash('success', 'Lieu modifié avec succès');
            $this->app->redirect($this->app->urlFor('adminCity'));
        }

    }

    public function add() {
        checkAdmin();
        $view = new CityEditView();
        $view->addVar('link',links());
        $view->addVar('session', $_SESSION);
        $view->addVar('formPost', $this->app->urlFor('adminCityAddPost'));
        $token = generateToken();
        $view->addVar('token', $token);
        $_SESSION['token'] = $token;
        $view->addVar('title', 'PhotoLocate - Administration');
        $env = $this->app->environment();
        $view->addVar('path',$env['SCRIPT_NAME']);
        echo $view->render();
    }

    public function addPost() {
        checkAdmin();
        $post = $this->app->request->post();

        if($_SESSION['token'] != $post['token']) {
            $this->app->flash('error', 'Bad token');
            $this->app->redirect($this->app->urlFor('adminCityAdd'));
        }

        $city = new City();
        $city->name = $post['name'];
        $city->longitude = $post['longitude'];
        $city->latitude = $post['latitude'];
        $city->zoom = $post['zoom'];
        $city->created_at = date('Y-m-d H:i:s');
        $city->save();

        $this->app->flash('success', 'Lieu ajouté avec succès');
        $this->app->redirect($this->app->urlFor('adminCity'));

    }

}