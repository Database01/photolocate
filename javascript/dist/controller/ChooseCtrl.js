// Generated by CoffeeScript 1.9.0
(function() {
  var app;

  app = angular.module('app');

  app.controller('CityCtrl', [
    '$scope', '$http', '$q', '$rootScope', '$location', function($scope, $http, $q, $rootScope, $location) {
      var modal, url;
      if (localStorage.username === void 0) {
        modal = {
          scope: $scope,
          status: true,
          title: "Erreur",
          message: "Nous sommes désolés, mais vous n'avez pas de nom d'utilsateur, veuillez en choisir un.",
          button1: "Redirection",
          pressed1: '/'
        };
        $rootScope.$broadcast('modal', modal);
      }
      $scope.username = localStorage.username;
      url = rootpath + "/api/cities";
      $http.get(url).success(function(data) {
        return $scope.cities = data.cities;
      }).error(function() {
        modal = {
          scope: $scope,
          status: true,
          title: "Erreur",
          message: "City list unreachful",
          button1: "Redirection",
          pressed1: '/'
        };
        return $rootScope.$broadcast('modal', modal);
      });
      return $scope.setCity = function($event) {
        localStorage.setItem('cityId', $event.currentTarget.dataset.city);
        return $location.path('/choose-difficulty');
      };
    }
  ]);

  app.controller('DifficultyCtrl', [
    '$scope', '$http', '$q', '$rootScope', '$location', function($scope, $http, $q, $rootScope, $location) {
      var getDifficulties, modal, url;
      if (localStorage.username === void 0) {
        modal = {
          scope: $scope,
          status: true,
          title: "Erreur",
          message: "Nous sommes désolés, mais vous n'avez pas de nom d'utilsateur, veuillez en choisir un.",
          button1: "Redirection",
          pressed1: '/'
        };
        $rootScope.$broadcast('modal', modal);
      }
      $scope.username = localStorage.username;
      if (localStorage.cityId === void 0) {
        modal = {
          scope: $scope,
          status: true,
          title: "Erreur",
          message: "Nous sommes désolés, mais vous n'avez pas choisi de lieu, veuillez en choisir une.",
          button1: "Redirection",
          pressed1: '/choose-city'
        };
        $rootScope.$broadcast('modal', modal);
      }
      url = rootpath + "/api/cities/" + localStorage.cityId;
      $http.get(url).success(function(data) {
        if (data.photos === void 0) {
          modal = {
            scope: $scope,
            status: true,
            title: "Erreur",
            message: "Nous sommes désolés, mais nous n'avons pas trouvé le lieu choisit.",
            button1: "Redirection",
            pressed1: '/choose-city'
          };
          return $rootScope.$broadcast('modal', modal);
        } else {
          return getDifficulties(data.photos.length);
        }
      }).error(function() {
        modal = {
          scope: $scope,
          status: true,
          title: "Erreur",
          message: "Difficulties list unreachful",
          button1: "Redirection",
          pressed1: '/choose-city'
        };
        return $rootScope.$broadcast('modal', modal);
      });
      getDifficulties = function(maxPhotos) {
        url = rootpath + "/api/difficulties";
        return $http.get(url).success(function(data) {
          var diff, diffs, _i, _len, _ref;
          diffs = [];
          _ref = data.difficulties;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            diff = _ref[_i];
            if (diff.nb_photo <= maxPhotos) {
              diffs.push(diff);
            }
          }
          if (diffs.length === 0) {
            modal = {
              scope: $scope,
              status: true,
              title: "Erreur",
              message: "Nous sommes désolés, mais nous n'avons pas de dfficultés correspondant à ce lieu.",
              button1: "Redirection",
              pressed1: '/choose-city'
            };
            return $rootScope.$broadcast('modal', modal);
          } else {
            return $rootScope.difficulties = diffs;
          }
        }).error(function() {
          modal = {
            scope: $scope,
            status: true,
            title: "Erreur",
            message: "Difficulties list unreachful",
            button1: "Redirection",
            pressed1: '/choose-city'
          };
          return $rootScope.$broadcast('modal', modal);
        });
      };
      return $scope.setDifficulty = function($event) {
        $scope.difficultyId = $event.currentTarget.dataset.difficulty;
        url = rootpath + "/play/games";
        return $http.post(url, {
          city: localStorage.cityId,
          difficulty: $scope.difficultyId,
          username: $scope.username
        }).success(function(data) {
          localStorage.setItem('token', JSON.stringify(data.token));
          localStorage.setItem('id_game', JSON.stringify(data.id_game));
          localStorage.setItem('id_user', JSON.stringify(data.id_user));
          localStorage.removeItem('progress');
          localStorage.removeItem('history');
          $rootScope.hasRefreshed = true;
          return $location.path('/games');
        }).error(function() {
          modal = {
            scope: $scope,
            status: true,
            title: "Erreur",
            message: "Nous sommes désolés, mais nous n'avons pas réussi à créer votre jeu avec cette carte et cette difficulté.",
            button1: "Redirection",
            pressed1: '/choose-city'
          };
          return $rootScope.$broadcast('modal', modal);
        });
      };
    }
  ]);

  app.directive('city', function() {
    return {
      restrict: 'E',
      templateUrl: rootpath + "/template/partial/city.html"
    };
  });

  app.directive('difficulty', function() {
    return {
      restrict: 'E',
      templateUrl: rootpath + "/template/partial/difficulty.html"
    };
  });

}).call(this);
