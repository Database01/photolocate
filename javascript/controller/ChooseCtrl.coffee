app = angular.module 'app'

#Controlleur lors de la sélection de la ville
app.controller 'CityCtrl', ['$scope', '$http', '$q', '$rootScope', '$location', ($scope, $http, $q, $rootScope, $location) ->

    if(localStorage.username == undefined)
        modal = {scope:$scope, status:true,title:"Erreur", message: "Nous sommes désolés, mais vous n'avez pas de nom d'utilsateur, veuillez en choisir un.", button1:"Redirection",pressed1:'/'}
        $rootScope.$broadcast('modal', modal)

    $scope.username = localStorage.username

    url = rootpath+"/api/cities"

    $http.get(url)
        .success (data) ->
            $scope.cities = data.cities
        .error ->
            modal = {scope:$scope, status:true,title:"Erreur", message: "City list unreachful", button1:"Redirection",pressed1:'/'}
            $rootScope.$broadcast('modal', modal)


    #Quand la personne clique sur la ville, le scope est modifié
    $scope.setCity = ($event) ->
        #On met dans le localStorage l'id de la ville
        localStorage.setItem 'cityId', $event.currentTarget.dataset.city

        #On redirige vers la page de choix de difficulté
        $location.path '/choose-difficulty'
]


#Controlleur lors de la sélection de la difficulté
app.controller 'DifficultyCtrl', ['$scope', '$http', '$q', '$rootScope', '$location', ($scope, $http, $q, $rootScope, $location) ->

    #Si le username n'est pas set, on redirige vers la connexion
    if(localStorage.username == undefined)
        modal = {scope:$scope, status:true,title:"Erreur", message: "Nous sommes désolés, mais vous n'avez pas de nom d'utilsateur, veuillez en choisir un.", button1:"Redirection",pressed1:'/'}
        $rootScope.$broadcast('modal', modal)

    $scope.username = localStorage.username

    #Si l cityId n'est pas set, on redirige vers la page de choix de la ville
    if localStorage.cityId == undefined
        modal = {scope:$scope, status:true,title:"Erreur", message: "Nous sommes désolés, mais vous n'avez pas choisi de lieu, veuillez en choisir une.", button1:"Redirection",pressed1:'/choose-city'}
        $rootScope.$broadcast('modal', modal)


    url = rootpath+"/api/cities/"+localStorage.cityId
    $http.get(url)
        .success (data) ->
            if data.photos == undefined
                modal = {scope:$scope, status:true,title:"Erreur", message: "Nous sommes désolés, mais nous n'avons pas trouvé le lieu choisit.", button1:"Redirection",pressed1:'/choose-city'}
                $rootScope.$broadcast('modal', modal)
            else
                getDifficulties(data.photos.length)
        .error ->
            modal = {scope:$scope, status:true,title:"Erreur", message: "Difficulties list unreachful", button1:"Redirection",pressed1:'/choose-city'}
            $rootScope.$broadcast('modal', modal)

    #On choope les difficultés et on prend que celles où il y a assez de photos pour lancer la partie
    getDifficulties = (maxPhotos) ->
        url = rootpath+"/api/difficulties"
        $http.get(url)
        .success (data) ->
            diffs = [];
            for diff in data.difficulties
                if diff.nb_photo <= maxPhotos
                    diffs.push diff

            if diffs.length == 0
                modal = {scope:$scope, status:true,title:"Erreur", message: "Nous sommes désolés, mais nous n'avons pas de dfficultés correspondant à ce lieu.", button1:"Redirection",pressed1:'/choose-city'}
                $rootScope.$broadcast('modal', modal)
            else
                $rootScope.difficulties = diffs
        .error ->
            modal = {scope:$scope, status:true,title:"Erreur", message: "Difficulties list unreachful", button1:"Redirection",pressed1:'/choose-city'}
            $rootScope.$broadcast('modal', modal)

    #Quand la personne clique sur la difficulté, le scope est modifié
    $scope.setDifficulty = ($event) ->

        #Set in localStorage difficulty Id
        $scope.difficultyId = $event.currentTarget.dataset.difficulty

        url = rootpath+"/play/games"
        #Si succès, on renvoie les données avec resolve au deferer, sinon, on les rejette au deferer
        $http.post(url, {city: localStorage.cityId, difficulty: $scope.difficultyId, username: $scope.username})
            .success (data) ->
                localStorage.setItem 'token', JSON.stringify data.token
                localStorage.setItem 'id_game', JSON.stringify data.id_game
                localStorage.setItem 'id_user', JSON.stringify data.id_user
                localStorage.removeItem 'progress'
                localStorage.removeItem 'history'
                $rootScope.hasRefreshed = true
                $location.path('/games')
            .error () ->
                modal = {scope:$scope, status:true,title:"Erreur", message: "Nous sommes désolés, mais nous n'avons pas réussi à créer votre jeu avec cette carte et cette difficulté.", button1:"Redirection",pressed1:'/choose-city'}
                $rootScope.$broadcast('modal', modal)
]

app.directive 'city', ->
    {
        restrict: 'E',
        templateUrl: rootpath+"/template/partial/city.html"
    }

app.directive 'difficulty', ->
    {
        restrict: 'E',
        templateUrl: rootpath+"/template/partial/difficulty.html"
    }
