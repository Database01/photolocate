<?php

namespace view;

class Home extends View {

    public function __construct() {
        $this->layout = "index.html.twig";
    }

}