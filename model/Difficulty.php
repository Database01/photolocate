<?php namespace model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Difficulty extends Model{

    use SoftDeletingTrait;
    public $table = 'Difficulty';
    public $idTable = 'id';
    public $timestamps = false;
    public $dates = ['deleted_at'];


    public function games() {
        return $this->belongsToMany('model\Game','id_difficulty');
    }
}