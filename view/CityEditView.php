<?php

namespace view;

class CityEditView extends View {

    public function __construct() {
        $this->layout = "admin-city-edit.html.twig";
    }

}