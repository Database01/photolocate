<?php
namespace controller;

use model\City;
use model\Difficulty;
use model\Game;
use model\Photo;
use model\User;

class APIController extends BaseController {

    public function createGame() {
        try {
            $response = $this->app->response();
            $request = $this->app->request();
            $body = $request->getBody();
            $param = json_decode($body);

            $cityId = $param->city;
            $city = City::find($cityId);
            if($city == null) {
                throw new \Exception('City not found');
            }

            $difficultyId = $param->difficulty;
            $difficulty = Difficulty::find($difficultyId);
            if($difficulty == null) {
                throw new \Exception('Difficulty not found');
            }

            $photos = Photo::where('id_city', '=', $city->id)->orderByRaw("RAND()")->take($difficulty->nb_photo)->get();
            if(sizeof($photos) < $difficulty->nb_photo) {
                throw new \Exception('Not enougth photos');
            }

            $game = new Game();
            $game->id_city = $cityId;
            $game->id_difficulty = $difficultyId;
            $game->created_at = date('Y-m-d H:i:s');
            $token = generateToken();
            $game->token = $token;

            $user = new User();
            $user->username = $param->username;
            $user->save();

            $game->id_user = $user->id;
            $game->save();

            foreach ($photos as $photo) {
                $game->photos()->attach($photo);
            }
            $game->save();

            $game->photos = $game->photos()->get();
            $game->city = $city;
            $game->difficulty = $difficulty;

            $data = array();
            $data['status'] = 200;
            $data['id_game'] = $game->id;
            $data['id_user'] = $user->id;
            $data['token'] = $token;
            $response->write(json_encode($data));
        } catch(\Exception $e) {
            $this->app->response()->status(400);
            $this->app->response()->header('X-Status-Reason', $e->getMessage());
        }
    }

    public function cities() {
        $cities = City::all();

        $response = $this->app->response();
        $data = array();
        $data['status'] = 200;
        $data['cities'] = $cities;
        $response->write(json_encode($data));
    }

    public function cityDetail($id) {
        $city = City::find($id);
        if($city == null) {
            $this->app->response()->status(400);
            $this->app->response()->header('X-Status-Reason', 'City not found');
            die;
        }
        $photos = $city->photos()->get();


        $response = $this->app->response();
        $data = array();
        $data['status'] = 200;
        $data['city'] = $city;
        $data['photos'] = $photos;
        $response->write(json_encode($data));
    }

    public function difficulties() {
        $difficulties = Difficulty::all();

        $response = $this->app->response();
        $data = array();
        $data['status'] = 200;
        $data['difficulties'] = $difficulties;
        $response->write(json_encode($data));
    }


    public function getGame($id){
        $response = array();
        $response['status'] = 200;
        try{
            $game = Game::find($id);
            if ($game == null) {
                $this->app->response->setStatus(404);
                $response['status'] = 404;
                $response['message'] = "The game $id doesn't exist";
            } else {
                $array_game = $game;
                $array_game['user'] = $game->user;
                $array_game['city'] = $game->city;
                $array_game['photos'] = $game->photos;
                $array_game['difficulty'] = $game->difficulty;
                $response['results'] = $array_game;

            }
        }catch(Exception $e){
            $response['status'] = 500;
            $this->app->response->setBody("");
            $this->app->response->setStatus(500);
        }
        echo json_encode($response);
    }

    public function updateGame($id){
        try{
            $request = $this->app->request();
            $body = $request->getBody();
            $param = json_decode($body);
            $score = $param->score;
            $response = array();
            $response['status'] = 200;

            $game = Game::find($id);
            if ($game == null || isset($game->finished_at)) {
                throw new \Exception("Game $id not found");
            } else {
                $game->finished_at = date('Y-m-d H:i:s');
                $game->score = $score;
                $game->save();
            }
        }catch(Exception $e){
            $response['status'] = 500;
            $this->app->response->setBody("");
            $this->app->response->setStatus(500);
        }
        echo json_encode($response);
    }

    public function getLeaderboard($id) {
        $response = array();
        $response['status'] = 200;
        try {
            $originalGame = Game::with('user')->find($id);

            if ($originalGame == null) {
                $this->app->response->setStatus(404);
                $response['status'] = 404;
                $response['message'] = "The game $id doesn't exist";
            }
            else {
                $id_diff = $originalGame->id_difficulty;
                $id_city = $originalGame->id_city;

                // On récupère les 10 meilleurs scores pour cette ville et difficulté
                $games = Game::where('id_difficulty', '=', $id_diff)->where('id_city', '=', $id_city)->whereNotNull('finished_at')->orderBy('score', 'desc')->orderBy('id', 'desc')->with('user')->get();
                $leaderboard=array();
                $playerboard=array();
                $key=null;
                foreach ($games as $k => $game) {
                    if ($game->user->id == $originalGame->user->id) {
                        $key = $k;
                        break;
                    }
                }

                try {
                    if ($key === null) {
                        throw new \Exception('Key not found');
                    }
                }
                catch (\Exception $e) {
                    $this->app->response->setStatus(500);
                    die;
                }

                for ($i = $key-5; $i<$key+5; $i++) {
                    if ($i>=sizeof($games)) {
                        break;
                    } elseif ($i<0) {
                        continue;
                    }
                    $playerboard[]=array('username' => $games[$i]->user->username, 'id' => $games[$i]->user->id, 'score' => $games[$i]->score, 'rank' => $i+1);
                }
                for ($i = 0; $i<10; $i++) {
                    if ($i>=sizeof($games)) {
                        break;
                    }
                    $leaderboard[]=array('username' => $games[$i]->user->username, 'id' => $games[$i]->user->id, 'score' => $games[$i]->score, 'rank' => $i+1);
                }

                $response['results']['leaderboard']=$leaderboard;
                $response['results']['playerboard']=$playerboard;

                // On récupère le rank du joueur de la partie courante
                $currentRank = Game::where('id_difficulty', '=', $id_diff)->where('id_city', '=', $id_city)->whereNotNull('finished_at')->where('score', '>', $originalGame->score)->count();
                $response['results']['player']=array('id' => $originalGame->user->id, 'username' => $originalGame->user->username, 'score' => $originalGame->score, 'rank' => $currentRank+1);

                // On récupère le nombre total de parties jouées pour cette ville et difficulté
                $totalPlays = Game::where('id_difficulty', '=', $id_diff)->where('id_city', '=', $id_city)->whereNotNull('finished_at')->count();
                $response['results']['game']=array('totalPlays' => $totalPlays, 'difficulty' => $originalGame->difficulty->label, 'city' => $originalGame->city->name);
            }
        }
        catch (Exception $e) {
            $response['status'] = 500;
            $this->app->response->setBody('');
            $this->app->response->setStatus(500);
        }
        echo json_encode($response);
    }
}