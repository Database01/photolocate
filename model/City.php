<?php namespace model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class City extends Model{

    use SoftDeletingTrait;
    public $table = 'City';
    public $idTable = 'id';
    public $timestamps = false;
    public $dates = ['deleted_at'];

    public function games() {
        return $this->belongsToMany('model\Game');
    }

    public function photos() {
        return $this->hasMany('model\Photo', 'id_city');
    }
}