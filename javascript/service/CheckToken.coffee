app = angular.module 'app'

app.factory 'checkToken', ['$location', '$http', ($location, $http) ->
    return (redirect) ->
        token = JSON.parse localStorage.token
        idGame = JSON.parse localStorage.id_game
        url = rootpath + "/play/games/" + idGame

        $http.get url
            .success (data) ->
                if data.results.token != token
                    $location.path redirect
            .error (error) ->
                $location.path redirect

]
