<?php namespace model;

use Illuminate\Database\Eloquent\Model;

class User extends Model{

    public $table = 'User';
    public $idTable = 'id';
    public $timestamps = false;


    public function games() {
        return $this->hasMany('model\Game', 'id_user');
    }

    public function photos(){
        return $this->hasMany('model\Photo','id_user');
    }
}