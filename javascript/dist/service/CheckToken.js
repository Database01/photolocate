// Generated by CoffeeScript 1.9.0
(function() {
  var app;

  app = angular.module('app');

  app.factory('checkToken', [
    '$location', '$http', function($location, $http) {
      return function(redirect) {
        var idGame, token, url;
        token = JSON.parse(localStorage.token);
        idGame = JSON.parse(localStorage.id_game);
        url = rootpath + "/play/games/" + idGame;
        return $http.get(url).success(function(data) {
          if (data.results.token !== token) {
            return $location.path(redirect);
          }
        }).error(function(error) {
          return $location.path(redirect);
        });
      };
    }
  ]);

}).call(this);
