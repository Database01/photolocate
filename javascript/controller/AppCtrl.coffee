app = angular.module 'app'

#On fait pas grand chose ici encore :p
app.controller 'AppCtrl', ['$scope', '$http', '$q', '$rootScope', '$location', ($scope, $http, $q, $rootScope, $location) ->

    ###
        How to use modal :
    if(localStorage.username == undefined)
        modal = {scope:$scope, status:true,title:"Modal Title", message: "Modal Message", button1:"Button1",button2:"Button2",pressed1:'/'}
        $rootScope.$broadcast('modal', modal)
    ###
    $scope.$on 'modal', (event, data) ->
        scope = data.scope
        scope.modal = {}
        scope.modal.status = true
        scope.modal.title = data.title
        scope.modal.message = data.message
        scope.modal.button1 = data.button1
        scope.modal.button2 = data.button2
        scope.modal.pressed1 = ->
            $location.path data.pressed1
        scope.modal.pressed2 = ->
            scope.modal.status=false

]

app.directive 'modal', ->
    {
        restrict: 'E',
        templateUrl: rootpath+"/template/partial/modal.html"
    }